import { Component, OnInit } from '@angular/core';
import { SigupService } from '../signup/sigup.service';


@Component({
  selector: 'app-viewdetails',
  templateUrl: './viewdetails.component.html',
  styleUrls: ['./viewdetails.component.css']
})
export class ViewdetailsComponent implements OnInit {
  userDetails:[];
  constructor(private service: SigupService) { }

  ngOnInit() {
    this.service.getUserdetails().subscribe((res:any) => {
      this.userDetails= res;
    });

  }

}
