import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})
export class LoginService {
  url = "http://localhost:3000/api/search";
  constructor(private http: HttpClient) { 
    
  }
  getUserEmail(loginData): Observable<any[]> {
    return this.http.post(this.url,loginData).
      pipe(map((res: any[]) => {
        console.log(res);
        return res;
      }));
  }
}
