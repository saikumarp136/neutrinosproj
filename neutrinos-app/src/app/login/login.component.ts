import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from './login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  login:FormGroup;
  validUser: any;
  constructor(private service:LoginService) { }
    router;
    check;
  ngOnInit() {
    this.login = new FormGroup(
      {
        email: new FormControl('',[Validators.required,Validators.email]),
        firstname: new FormControl('',Validators.required),
        lastname: new FormControl('',Validators.required),
        date: new FormControl('',Validators.required)
      }
    );
    }
 async onClick(){
    console.log(this.login.value);
     this.service.getUserEmail(this.login.value).subscribe((res:any) => {
      this.check= res;
      console.log(this.check);

      if(this.check!=null)
      {
        this.validUser=true;
        console.log(this.check.firstname+" Login Successfully....")
      }
      else
      {
        this.validUser=false;
        console.log("Invalid User!")
      }
   });
  

}
}