import { TestBed } from '@angular/core/testing';

import { SigupService } from './sigup.service';

describe('SigupService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SigupService = TestBed.get(SigupService);
    expect(service).toBeTruthy();
  });
});
