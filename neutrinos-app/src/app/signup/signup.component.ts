import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { SigupService } from './sigup.service';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
  
adduser:FormGroup;
  constructor(private service:SigupService) { }

  ngOnInit() {
    this.adduser = new FormGroup(
      {
        firstname: new FormControl("",Validators.required),
        lastname: new FormControl("",Validators.required),
        age: new FormControl("",Validators.required),
        phone: new FormControl("",Validators.required),        
        email: new FormControl("",[Validators.required,Validators.required]),
        date: new FormControl('',Validators.required)
        //  gender: new FormControl("",Validators.required),
      }
    )
  }
  onClick(){
    // console.log(this.adduser.value);
    let status= this.service.createUser(this.adduser.value);
    console.log("Status : "+status)
  }

}
