import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SigupService {
  url = "http://localhost:3000/api/user";
  constructor(private http: HttpClient) { }
  createUser(data:any){
    let userData = new Promise((resolve, reject) => {
      this.http.post(this.url, data).toPromise().then(
        (res: any) => {
          console.log(res);
          resolve(res);
        }
      ).catch((err: any) => {
        console.log(err)
        reject(err)
      })
    })
    return userData as Promise<any>;
  }
  getUserdetails(): Observable<any[]> {
    return this.http.get(this.url).
      pipe(map((res: any[]) => {
        console.log(res);
        return res;
      }));
  }
}
