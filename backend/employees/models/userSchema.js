var mongoose = require('./mongo');
var autoIncrement = require('mongoose-auto-increment');

var schema = mongoose.Schema;
const userDB = new schema({
    id: Number,
    firstname: String,
    lastname: String,
    email: String,
    phone: String,
    age: Number,
    // gender: String,
    dateofbirth: String,
});

userDB.plugin(autoIncrement.plugin, { model: 'userDB', field: 'id', startAt: 1 });
module.exports = mongoose.model('userDB', userDB);

