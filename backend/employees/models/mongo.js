var mongoose = require('mongoose');
var process = require('process');
var autoIncrement = require('mongoose-auto-increment')
const Config = require('../config/mongoConfig');

var url = Config.url;
var options = Config.options;

mongoose.connect(url, options).then(() => {
    console.log("connected to port" + url)
})

process.on("SIGINT", () => {
    mongoose.connection.close(() => {
        console.log('connection closed');
        process.exit(0);
    })
})
autoIncrement.initialize(mongoose.connection)
module.exports = mongoose;


