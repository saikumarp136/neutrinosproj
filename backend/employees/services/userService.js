var employee = require('../models/userSchema');

exports.saveEmployee = function (emploeeData, callback) {
    var newEmployee = employee(emploeeData)

    newEmployee.save((err, data) => {
        if (!err) {
            console.log("Employee data saved successfully");
        }
        else {
            console.log("not saved data")
        }
        callback(err, data);
    })
}


exports.getEmployees = function (callback) {
    employee.find((err, data) => {
        if (!err) {
            console.log("Get all Data");
        }
        else {
            console.log("No data found");
        }
        callback(err, data);
    })
}

exports.getEmployeById = function (id, callback) {
    employee.findOne({ 'id': parseInt(id) }, (err, data) => {
        if (!err) {
            console.log("Get data by id");
        }
        else {
            console.log("No data found");
        }
        callback(err, data);
    })
}


exports.updateEmployeeById = function (id, updateData, callback) {
    employee.findOneAndUpdate({ 'id': parseInt(id) }, updateData, (err, data) => {
        if (!err) {
            console.log("updated successfully");
        }
        else {
            console.log("No data found");
        }
        callback(err, data);
    })
}

exports.deleteEmployeById = function (id, callback) {
    employee.findOneAndDelete({ 'id': parseInt(id) }, (err, data) => {
        if (!err) {
            console.log("Deleted successfully");
        }
        else {
            console.log("No data found");
        }
        callback(err, data);
    })
}