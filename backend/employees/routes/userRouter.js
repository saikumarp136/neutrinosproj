var express = require("express");
var service = require('../services/userService');
var serService = require('../services/searcService');

var router = express.Router();


router.post('/', (req, res, next) => {
    service.saveEmployee(req.body, (err, data) => {
        if (!err) {
            console.log(data);
            res.json(data);
        }
        else {
            console.log(err);
            res.json(err);
        }
    })
})

router.get('/', (req, res, next) => {

    service.getEmployees((err, data) => {
        if (!err) {
            console.log(data);
            res.json(data);
        }
        else {
            console.log(err);
            res.json(err);
        }
    })

})

router.get('/:id', (req, res, next) => {

    service.getEmployeById(req.params.id, (err, data) => {
        if (!err) {
            console.log(data);
            res.json(data);
        }
        else {
            console.log(err);
            res.json(err);
        }
    })

})

router.get('/', (req, res, next) => {

    serService.getEmployeByEmail(req.body, (err, data) => {
        if (!err) {
            console.log(data);
            res.json(data);
        }
        else {
            console.log(err);
            res.json(err);
        }
    })

})

router.put('/:id', (req, res, next) => {

    service.updateEmployeeById(req.params.id, req.body, (err, data) => {
        if (!err) {
            console.log(data);
            res.json(data);
        }
        else {
            console.log(err);
            res.json(err);
        }
    })

})

router.delete('/:id', (req, res, next) => {
    service.deleteEmployeById(req.params.id, (err, data) => {
        if (!err) {
            console.log(data);
            res.json(data);
        }
        else {
            console.log(err);
            res.json(err);
        }
    })

})

module.exports = router;